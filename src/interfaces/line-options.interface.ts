export interface LineOptions {
  searchFiles: string;
  dictionaryPath: string;
  ignoreFiles: string[];
}
