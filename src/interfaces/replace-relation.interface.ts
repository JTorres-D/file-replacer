export interface ReplaceRelation {
  from: string;
  to: string;
}
