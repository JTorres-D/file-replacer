import { LineOptions } from './interfaces';
import {
  commanderOpts,
  generateOptions,
  getReplaceRelations,
  replacer,
  trimPaths,
} from './utils';

const options: LineOptions = commanderOpts();
const dictionary = getReplaceRelations(options.dictionaryPath);
const relationOptions = generateOptions(dictionary);
const ignoreFiles = trimPaths(options.ignoreFiles || []);

let optionsFrom = relationOptions.optionsFrom;
let optionsTo = relationOptions.optionsTo;

replacer(options.searchFiles, optionsFrom, optionsTo, ignoreFiles || []);
console.log('File replacer JS Done!');
