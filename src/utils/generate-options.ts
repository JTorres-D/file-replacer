import { ReplaceRelation } from '../interfaces';

export function generateOptions(dictionary: ReplaceRelation[]): {
  optionsFrom: RegExp[];
  optionsTo: string[];
} {
  let optionsFrom = new Array(dictionary.length);
  let optionsTo = new Array(dictionary.length);

  for (let i = 0; i < dictionary.length; i++) {
    optionsFrom[i] = new RegExp(`${dictionary[i].from}`, 'g');
    optionsTo[i] = dictionary[i].to;
  }

  return { optionsFrom, optionsTo };
}
