import { program } from 'commander';
import { LineOptions } from '../interfaces';

export function commanderOpts(): LineOptions {
  program.requiredOption(
    '-s, --search-files <path>',
    'Path where files to search and replace are located, example: ./dist/**.js'
  );
  program.requiredOption(
    '-d, --dictionary-path <path>',
    'Dictionary with from-to relation, example: [{"from": "", "to": ""}]'
  );
  program.option(
    '-i, --ignore-files [paths...]',
    'Ignore files to avoid search and replacement, example: ./modules/**.js'
  );
  program.parse();
  return program.opts();
}
