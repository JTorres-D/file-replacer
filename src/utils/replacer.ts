import { sync } from 'replace-in-file';

export function replacer(
  searchFiles: string,
  optionsFrom: RegExp[],
  optionsTo: string[],
  ignore: string[]
) {
  sync({
    files: searchFiles,
    from: optionsFrom,
    to: optionsTo,
    ignore,
  });
}
