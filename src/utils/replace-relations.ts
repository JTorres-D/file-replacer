import { readFileSync } from 'fs';
import { ReplaceRelation } from '../interfaces';

export function getReplaceRelations(dictionaryPath: string): ReplaceRelation[] {
  const dictionary: any = readFileSync(dictionaryPath);
  const dictionaryJSON: ReplaceRelation[] = JSON.parse(dictionary);
  return dictionaryJSON;
}
