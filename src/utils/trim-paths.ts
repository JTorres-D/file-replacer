export function trimPaths(paths: string[]): string[] {
  return paths.map((i) => i.trim());
}
