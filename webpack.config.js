const path = require('path');
('use strict');
module.exports = {
  entry: './build/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'file-replacer.js',
  },
  target: 'node',
  mode: 'production',
};
