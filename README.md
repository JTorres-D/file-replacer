# File Replacer

### JSON Dictionary

Provide JSON file, in parameter `--dictionary-path <path>` or `-d <path>`
Example:

```
-d ./test/env/production.env.json
```

`preprod.env.json`:

```
[
    {
        "from": "STRING_EXAMPLE",
        "to": "REPLACE_VALUE",
    },
    {
        "from": "STRING2_EXAMPLE",
        "to": "REPLACE_VALUE2",
    },
    {
        "from": "STRING3_EXAMPLE",
        "to": "REPLACE_VALUE3",
    },
    ...
]
```

### Search Files

To replace those values on files provided in `--search-files <path>` or or `-s <path>`

Example:

```
-s ./test/*.js
```

### Ignore Files

To ignore files, provide pattern in parameter `--ignore-files [paths...]` or `-i [paths...]`

```
-i ./test/runtime.js ./test/ignore.js
```

## Full Example

````
node .\file-replacer.js -s ./test/\*.js -d ./test/env/production.env.json -i ./test/runtime.js ./test/ignore.js```
````
